# DISCONTINUED

I had a great time working on this mod but I've moved on to a new project
called Omnimod which aims to be everything I'd want to mod all in one place,
including the Berserker and other classes too.

To follow the project, see my Omnimod repo:
https://gitlab.com/michaeldrotar/clicker-heroes-2-omnimod


# Clicker Heroes 2 Mod : Berserker

This repo and document are a work-in-progress but serve to share some very early development of a new character class mod for Clicker Heroes 2.
Certain elements of the game are still locked -- for instance, types of stats can't be added or removed -- so there may exist some things that don't make much sense or work quite correctly with this class.


## Versioning

This chart will help to align versions of Clicker Heroes with the latest version of this mod that's supported on that version.


| Clicker Heroes 2 Version | Berserker Version |
| ------------------------:|:----------------- |
|                    v0.08 | [v0.5.0][]        |

[v0.5.0]: /../raw/v0.5.0/Berserker.swf


## Getting Started

<img src="screenshots/character-create.png" align="right" width="200">

- Ensure Clicker Heroes version and Berkser version align according to the [Versioning](#versioning) chart
- Download `Berserker.swf` from the repo to your `mods/active` folder
- Add `Berserker.swf` to a new line in your `mods.txt` file
- Run the game
- Create a new Berserker character

For more information, including where to find the above-mentioned files and folder for your OS, refer to the official [Installing Mods](https://www.clickerheroes2.com/installing_mods.php) instructions.


## Screenshots

These images may not be completely up-to-date but help to visually represent some of the features of the game.

![](screenshots/berserk.png)
![](screenshots/bash.png)
![](screenshots/shove.png)


## More Information
If you'd like to learn more about the Berserker's planned design, check out the [Design Document](https://gitlab.com/michaeldrotar/clicker-heroes-2-berserker/wikis/design-document) in the wiki.

To follow implementation details, please refer to the [Changelog](CHANGELOG.md)

Questions? Bugs? Feedback? Open an [issue](https://gitlab.com/michaeldrotar/clicker-heroes-2-berserker/issues) or reach me at [/u/michaeldrotar](https://www.reddit.com/user/michaeldrotar) on reddit.
