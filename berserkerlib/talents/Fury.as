package berserkerlib.talents
{
	import berserkerlib.TalentExtension;
	import berserkerlib.Tooltip;
	import models.Character;

	public class Fury extends TalentExtension
	{
		public var partialEnergy:Number = 0.0;
		public var energyPerSecond:Number = 1;

		public function Fury()
		{
			super();
			name = 'Fury';
			description = 'Berserk generates { energyPerSecond | energy } energy per second while in combat.';
		}

		public function berserkDescriptionHook():Array
		{
			var talents:Array = next();
			if (enabled)
			{
				talents.push(Tooltip.format('{ "Fury:" | label } Generates { energyPerSecond | energy } energy per second while in combat.', this));
			}
			return talents;
		}

		public function characterUpdateHook(dt:int):void
		{
			next(dt);
			if (character.state === Character.STATE_COMBAT)
			{
				if (enabled)
				{
					partialEnergy += energyPerSecond * (dt / 1000);
					var fullEnergy:int = Math.floor(partialEnergy);
					if (fullEnergy > 0)
					{
						character.addEnergy(fullEnergy, false);
						partialEnergy -= fullEnergy;
					}
				}
			}
		}

		public function onExtensionsCreated():void
		{
			hook(extensions.berserk, 'getTalentDescriptions', berserkDescriptionHook);
		}

		public function onCharacterCreated():void
		{
			hook(character, 'update', characterUpdateHook);
		}

		public function onCharacterStarted():void
		{
			partialEnergy = 0.0;
		}
	}
}
