package berserkerlib
{
	import com.playsaurus.utils.StringFormatter;
	import mx.utils.StringUtil;

	public class Tooltip
	{
		public static function format(message:String, ... args):String
		{
			var substitutions:Array = args;
			var owner:* = args[0];

			message = message.replace(/(?<!{)\{(.+?)\}(?!})/g, function(... args):String
			{
				var parts:Array = args[1].split('|');
				var first:String = StringUtil.trim(parts[0]);
				var value:*;

				if (first.match(/^(['"]).+\1$/))
				{
					value = first.substring(1, first.length - 1);
				}
				else if (isNaN(Number(first)))
				{
					if (owner.hasOwnProperty(first))
					{
						value = owner[first];
					}
				}
				else
				{
					value = substitutions[Number(first)];
				}

				if (value is Function)
				{
					value = value();
				}

				if (value === undefined)
				{
					return args[0];
				}

				for (var i:int = 1; i < parts.length; i++)
				{
					var part:String = StringUtil.trim(parts[i]);
					if (!(part in Tooltip))
					{
						return args[0];
					}

					value = Tooltip[part](value);
				}

				return number(value);
			});

			message = message.replace(/\{\{/g, '{').replace(/\}\}/g, '}');

			return message;
		}

		public static function distance(value:Number):String
		{
			var unit:String = 'monster position';
			value = value / Util.MONSTER_POSITION_DISTANCE;
			var text:String = number(value);
			if (text != '1')
			{
				unit += 's';
			}
			return text + ' ' + unit;
		}

		public static function duration(value:Number):String
		{
			var unit:String = 'second';
			value = value / 1000;
			if (value > 60)
			{
				value = value / 60;
				unit = 'minute';
			}
			if (value > 60)
			{
				value = value / 60;
				unit = 'hour';
			}
			var text:String = number(value);
			if (text != '1')
			{
				unit += 's';
			}
			return text + ' ' + unit;

		}

		public static function number(value:*):*
		{
			if (value is Number)
			{
				return Number(value.toFixed(2)).toString();
			}
			if (value.toString)
			{
				return value.toString();
			}
			return value;
		}

		public static function invert(value:*):*
		{
			if (value is Number)
			{
				return value * -1;
			}
			return value;
		}

		public static function percent(value:*):*
		{
			if (value is Number)
			{
				return number(value * 100) + '%';
			}
			return value;
		}

		public static function damage(text:*):String
		{
			return StringFormatter.colorize(number(text), '#dd7777');
		}

		public static function energy(text:*):String
		{
			return StringFormatter.colorize(number(text), '#dddd77');
		}

		public static function label(text:*):String
		{
			return StringFormatter.colorize(number(text), '#bbbbbb');
		}

		public static function mana(text:*):String
		{
			return StringFormatter.colorize(number(text), '#7eeaff');
		}

		public static function value(text:*):String
		{
			return StringFormatter.colorize(number(text), '#77dd77');
		}
	}
}