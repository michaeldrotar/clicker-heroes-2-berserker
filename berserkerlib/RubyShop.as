package berserkerlib
{
	import berserkerlib.Extension;
	import heroclickerlib.CH2;
	import models.Character;
	import models.RubyPurchase;

	public class RubyShop extends Extension
	{
		public var duration:Number = 5 * 60 * 1000;
		public var cooldown:Number = 20 * 60 * 1000;
		public var maxItems:Number = 3;

		public function RubyShop()
		{
			duration = 30 * 1000;
			cooldown = 1 * 60 * 1000;
		}

		public function generate():void
		{
			character.currentRubyShop = [];
			trigger('onRubyShopGenerating');

			var option:RubyPurchase;
			var priority:int;
			var tries:int = 0;
			while (character.currentRubyShop.length < maxItems && tries < 10)
			{
				tries++;

				option = null;
				priority = character.currentRubyShop.length + 1;
				while (priority <= maxItems)
				{
					option = character.getRandomRubyPurchase(priority);
					if (option)
					{
						break;
					}
					priority++;
				}

				if (option)
				{
					for (var index:int = 0; index < character.currentRubyShop.length; index++)
					{
						if (character.currentRubyShop[index].name === option.name)
						{
							continue;
						}
					}

					character.currentRubyShop.push(option);
				}
			}

			trigger('onRubyShopGenerated');
		}

		public function shouldActivate():Boolean
		{
			return character.timeSinceLastRubyShopAppearance > cooldown && !CH2.world.isBossZone(character.currentZone) && !character.didFinishWorld && character.rubies >= 10;
		}

		public function shouldDeactivate():Boolean
		{
			return character.timeSinceLastRubyShopAppearance > duration || CH2.world.isBossZone(character.currentZone) || character.didFinishWorld;
		}

		public function onCharacterCreated():void
		{
			character.generateRubyShop = generate;
			character.shouldRubyShopDeactivate = shouldDeactivate;
			character.shouldRubyShopActivate = shouldActivate;
			character.populateRubyPurchaseOptions = Util.noop;
		}
	}
}