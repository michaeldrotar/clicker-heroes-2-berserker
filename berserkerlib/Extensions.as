package berserkerlib
{
	import berserkerlib.Data;
	import berserkerlib.IconViewer;
	import berserkerlib.Migrations;
	import berserkerlib.RubyShop;
	import berserkerlib.SkillTree;
	import berserkerlib.character.CharacterAttack;
	import berserkerlib.gems.items.BuyCheapestItemGem;
	import berserkerlib.gems.items.UpgradeCheapestItemFullGem;
	import berserkerlib.gems.items.UpgradeCheapestItemHalfGem;
	import berserkerlib.gems.skills.BashSkillGem;
	import berserkerlib.gems.skills.CleaveSkillGem;
	import berserkerlib.gems.skills.EviscerateSkillGem;
	import berserkerlib.gems.skills.RuptureSkillGem;
	import berserkerlib.gems.skills.ShoveSkillGem;
	import berserkerlib.gems.skills.SliceAndDiceSkillGem;
	import berserkerlib.nodeTypes.IncreasePower;
	import berserkerlib.nodeTypes.IncreasePowerBig;
	import berserkerlib.nodeTypes.IncreasePowerSuper;
	import berserkerlib.nodeTypes.IncreasePrecision;
	import berserkerlib.nodeTypes.IncreasePrecisionBig;
	import berserkerlib.nodeTypes.IncreasePrecisionSuper;
	import berserkerlib.nodeTypes.IncreaseSpeed;
	import berserkerlib.nodeTypes.IncreaseSpeedBig;
	import berserkerlib.nodeTypes.IncreaseSpeedSuper;
	import berserkerlib.rubyShop.bonuses.LevelUpBonus;
	import berserkerlib.rubyShop.bonuses.SkillPointBonus;
	import berserkerlib.rubyShop.bonuses.SkillTreeResetBonus;
	import berserkerlib.rubyShop.catalogs.ItemGemCatalog;
	import berserkerlib.rubyShop.catalogs.SkillGemCatalog;
	import berserkerlib.rubyShop.catalogs.TimingStoneCatalog;
	import berserkerlib.skills.Bash;
	import berserkerlib.skills.Berserk;
	import berserkerlib.skills.Cleave;
	import berserkerlib.skills.Eviscerate;
	import berserkerlib.skills.Rupture;
	import berserkerlib.skills.Shove;
	import berserkerlib.skills.SliceAndDice;
	import berserkerlib.stats.Power;
	import berserkerlib.stats.Precision;
	import berserkerlib.stats.Speed;
	import berserkerlib.stones.timing.AlwaysStone;
	import berserkerlib.stones.timing.LongWaitStone;
	import berserkerlib.stones.timing.LongerWaitStone;
	import berserkerlib.stones.timing.LongestWaitStone;
	import berserkerlib.stones.timing.MediumWaitStone;
	import berserkerlib.stones.timing.ShortWaitStone;
	import berserkerlib.stones.timing.ShorterWaitStone;
	import berserkerlib.stones.timing.ShortestWaitStone;
	import berserkerlib.talents.Fury;
	import berserkerlib.talents.Rage;
	import flash.utils.describeType;

	public class Extensions
	{
		public var data:Data = new Data();
		public var migrations:Migrations = new Migrations();

		public var characterAttack:CharacterAttack = new CharacterAttack();

		public var bashSkillGem:BashSkillGem = new BashSkillGem();
		public var shoveSkillGem:ShoveSkillGem = new ShoveSkillGem();
		public var cleaveSkillGem:CleaveSkillGem = new CleaveSkillGem();
		//public var ruptureSkillGem:RuptureSkillGem = new RuptureSkillGem();
		//public var sliceAndDiceSkillGem:SliceAndDiceSkillGem = new SliceAndDiceSkillGem();
		//public var eviscerateSkillGem:EviscerateSkillGem = new EviscerateSkillGem();
		public var skillGemCatalog:SkillGemCatalog = new SkillGemCatalog();

		public var buyCheapestItemGem:BuyCheapestItemGem = new BuyCheapestItemGem();
		public var upgradeCheapestItemFullGem:UpgradeCheapestItemFullGem = new UpgradeCheapestItemFullGem();
		public var upgradeCheapestItemHalfGem:UpgradeCheapestItemHalfGem = new UpgradeCheapestItemHalfGem();
		public var itemGemCatalog:ItemGemCatalog = new ItemGemCatalog();

		public var alwaysStone:AlwaysStone = new AlwaysStone();
		public var shortestWaitStone:ShortestWaitStone = new ShortestWaitStone();
		public var shorterWaitStone:ShorterWaitStone = new ShorterWaitStone();
		public var shortWaitStone:ShortWaitStone = new ShortWaitStone();
		public var mediumWaitStone:MediumWaitStone = new MediumWaitStone();
		public var longWaitStone:LongWaitStone = new LongWaitStone();
		public var longerWaitStone:LongerWaitStone = new LongerWaitStone();
		public var longestWaitStone:LongestWaitStone = new LongestWaitStone();
		public var timingStoneCatalog:TimingStoneCatalog = new TimingStoneCatalog();

		public var levelUpBonus:LevelUpBonus = new LevelUpBonus();
		public var skillPointBonus:SkillPointBonus = new SkillPointBonus();
		public var skillTreeResetBonus:SkillTreeResetBonus = new SkillTreeResetBonus();

		public var rubyShop:RubyShop = new RubyShop();

		public var berserk:Berserk = new Berserk();
		public var bash:Bash = new Bash();
		public var shove:Shove = new Shove();
		public var cleave:Cleave = new Cleave();
		//public var rupture:Rupture = new Rupture();
		//public var sliceAndDice:SliceAndDice = new SliceAndDice();
		//public var eviscerate:Eviscerate = new Eviscerate();

		public var rage:Rage = new Rage();
		public var fury:Fury = new Fury();

		public var power:Power = new Power();
		public var precision:Precision = new Precision();
		public var speed:Speed = new Speed();

		public var increasePower:IncreasePower = new IncreasePower();
		public var increasePowerBig:IncreasePowerBig = new IncreasePowerBig();
		public var increasePowerSuper:IncreasePowerSuper = new IncreasePowerSuper();
		public var increaseSpeed:IncreaseSpeed = new IncreaseSpeed();
		public var increaseSpeedBig:IncreaseSpeedBig = new IncreaseSpeedBig();
		public var increaseSpeedSuper:IncreaseSpeedSuper = new IncreaseSpeedSuper();
		public var increasePrecision:IncreasePrecision = new IncreasePrecision();
		public var increasePrecisionBig:IncreasePrecisionBig = new IncreasePrecisionBig();
		public var increasePrecisionSuper:IncreasePrecisionSuper = new IncreasePrecisionSuper();

		public var skillTree:SkillTree = new SkillTree();

		//public var iconViewer:IconViewer = new IconViewer();

		private var _extensionKeys:Array;

		public function Extensions()
		{

		}

		public function get extensionKeys():Array
		{
			if (!_extensionKeys)
			{
				var name:String;
				var pos:Number;
				var index:int;

				var data:Array = [];
				var variablesXml:XMLList = describeType(this)..variable;
				for (index = 0; index < variablesXml.length(); index++)
				{
					name = variablesXml[index].@name;
					pos = parseInt(variablesXml[index]..metadata..arg.@value, 10);
					if (isNaN(pos)) // safety check
					{
						pos = index;
					}
					data.push({name: name, pos: pos});
				}
				data.sortOn('pos', Array.NUMERIC);

				_extensionKeys = [];
				for (index = 0; index < data.length; index++)
				{
					_extensionKeys.push(data[index]['name']);
				}
			}
			return _extensionKeys;
		}

		public function forEach(callback:Function):void
		{
			for (var i:uint = 0; i < extensionKeys.length; i++)
			{
				callback(this[extensionKeys[i]]);
			}
		}
	}
}
