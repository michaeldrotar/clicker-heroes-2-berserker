package berserkerlib
{

	public class Hooks
	{
		protected var _hooks:Array = [];

		public function add(object:Object, methodName:String, callback:Function, extension:Object):void
		{
			var hook:Object = _findHookByObject(object);
			if (!hook)
			{
				hook = {object: object, methods: {}};
				_hooks.push(hook);
			}

			var methods:Array = hook.methods[methodName];
			if (!methods)
			{
				methods = [];
				methods.push(object[methodName]);
				hook.methods[methodName] = methods;

				object[methodName] = function(... args):*
				{
					return methods[0].apply(object, args);
				};
			}

			var nextIndex:uint = methods.length;
			var hookedCallback:Function = function(... args):*
			{
				extension.next = methods[nextIndex];
				return callback.apply(object, args);
			};

			var original:Function = methods.pop();
			methods.push(hookedCallback);
			methods.push(original);
		}

		protected function _findHookByObject(object:Object):Object
		{
			for (var index:uint = 0; index < _hooks.length; index++)
			{
				if (_hooks[index].object === object)
				{
					return _hooks[index];
				}
			}
			return null;
		}
	}
}