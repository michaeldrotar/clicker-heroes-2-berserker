package berserkerlib
{
	import berserkerlib.Extension;

	public class Data extends Extension
	{
		public function get keys():Array
		{
			var key:String;
			var keys:Array = [];
			for (key in character.traits)
			{
				keys.push(key);
			}
			return keys;
		}

		public function getArray(key:String):Array
		{
			return _getValue(key) as Array;
		}

		public function setArray(key:String, value:Array):void
		{
			_setValue(key, value);
		}

		public function getBoolean(key:String):Boolean
		{
			return _getValue(key) as Boolean;
		}

		public function setBoolean(key:String, value:Boolean):void
		{
			_setValue(key, value);
		}

		public function getNumber(key:String):Number
		{
			return _getValue(key) as Number;
		}

		public function setNumber(key:String, value:Number):void
		{
			_setValue(key, value);
		}

		public function adjustNumber(key:String, value:Number):void
		{
			_setValue(key, _getValue(key) + value);
		}

		public function getString(key:String):String
		{
			return _getValue(key) as String;
		}

		public function setString(key:String, value:String):void
		{
			_setValue(key, value);
		}

		public function removeKey(key:String):void
		{
			_remove(key);
		}

		public function removeMatched(search:RegExp):void
		{
			var key:String;
			var keys:Array = [];
			for ( key in character.traits) {
				if (key.match(search)) {
					keys.push(key);
				}
			}
			for each (key in keys) {
				removeKey(key);
			}
		}

		private function _getValue(key:String):*
		{
			return character.traits[key];
		}

		private function _setValue(key:String, value:*):void
		{
			character.traits[key] = value;
		}

		private function _remove(key:String):void
		{
			delete character.traits[key];
		}
	}
}