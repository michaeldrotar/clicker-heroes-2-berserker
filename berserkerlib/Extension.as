package berserkerlib
{
	import berserkerlib.Util;
	import com.playsaurus.random.Random;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;
	import heroclickerlib.CH2;
	import models.Character;

	public class Extension
	{
		public var next:Function;

		protected var _id:String;

		public function define(name:String, value:* = null):void
		{
			this[name] = function(... args):*
			{
				if (value is Function)
				{
					return value.apply(self, args);
				}
				return value;
			};
		}

		public function get character():Character
		{
			return mod.currentCharacter;
		}

		public function get data():Data
		{
			return extensions.data;
		}

		public function get extensions():Extensions
		{
			return mod.extensions;
		}

		public function get id():String
		{
			if (!_id)
			{
				var baseClassName:String = getQualifiedClassName(this);
				_id = Util.underscore(baseClassName.replace(/^.+::/, ''));

				var parentClassName:String = baseClassName;
				var parentId:String;
				while (true)
				{
					parentClassName = getQualifiedSuperclassName(getDefinitionByName(parentClassName));
					if (parentClassName === 'Object')
					{
						break;
					}
					parentId = Util.underscore(parentClassName.replace(/(^.+::|Extension$)/g, ''));
					_id = _id.replace(parentId, '');
					_id = parentId + '_' + _id;
				}

				_id = Util.toId([mod.modName, _id]);
			}
			return _id;
		}

		public function get mod():BerserkerMain
		{
			return BerserkerMain.instance;
		}

		public function get roller():Random
		{
			return CH2.roller.modRoller;
		}

		public function get self():Extension
		{
			return this;
		}

		public function call(... args):*
		{
			if (args[0] is String)
			{
				args.unshift(this);
			}
			return mod.call.apply(mod, args);
		}

		public function hook(object:Object, methodName:String, callback:Function):void
		{
			if (object === this && !(methodName in this))
			{
				define(methodName, Util.noop);
			}
			mod.hooks.add(object, methodName, callback, this);
		}

		public function trace(... args):void
		{
			Util.trace.apply(Util, args);
		}

		public function trigger(name:String, ... args):void
		{
			args.unshift(name);
			mod.trigger.apply(mod, args);
		}
	}
}
