package berserkerlib
{
	import ui.IdleHeroUIManager;

	public class StoneCatalogExtension extends CatalogExtension
	{
		public function StoneCatalogExtension()
		{
			super();
			iconId = 1;
			price = 10;
			priority = 2;
		}

		override public function purchaseChoice(choice:String):void
		{
			character.automator.unlockStone(choice);
			character.hasUnlockedAutomator = true;
			IdleHeroUIManager.instance.mainUI.mainPanel.refreshOpenTab();
		}
	}
}