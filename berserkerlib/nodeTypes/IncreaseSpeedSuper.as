package berserkerlib.nodeTypes
{

	public class IncreaseSpeedSuper extends IncreaseSpeed
	{
		public function IncreaseSpeedSuper()
		{
			super();
			name = 'Speed x10';
			amount = 10;
			quality = QUALITY_DELUXE;
		}
	}
}
