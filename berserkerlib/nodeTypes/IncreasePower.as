package berserkerlib.nodeTypes
{
	import berserkerlib.NodeTypeExtension;

	public class IncreasePower extends NodeTypeExtension
	{
		public var amount:Number;

		public function IncreasePower()
		{
			super();
			name = 'Power';
			description = 'Multiplies all damage by { damageBonus | percent | value }.';
			icon = 'damageWeapon';
			amount = 1;
		}

		public function get damageBonus():Number
		{
			return extensions.power.damageMultiplier(amount);
		}

		public function purchase():void
		{
			extensions.power.amount += amount;
		}
	}
}