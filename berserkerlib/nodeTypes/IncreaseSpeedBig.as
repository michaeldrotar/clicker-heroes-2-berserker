package berserkerlib.nodeTypes
{

	public class IncreaseSpeedBig extends IncreaseSpeed
	{
		public function IncreaseSpeedBig()
		{
			super();
			name = 'Speed x3';
			amount = 3;
			quality = QUALITY_SPECIAL;
		}
	}
}
