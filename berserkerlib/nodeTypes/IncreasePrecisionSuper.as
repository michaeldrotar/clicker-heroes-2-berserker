package berserkerlib.nodeTypes
{

	public class IncreasePrecisionSuper extends IncreasePrecision
	{
		public function IncreasePrecisionSuper()
		{
			super();
			name = 'Precision x10';
			amount = 10;
			quality = QUALITY_DELUXE;
		}
	}
}
