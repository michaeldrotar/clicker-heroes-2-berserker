package berserkerlib.stones.timing
{
	import berserkerlib.StoneExtension;

	public class MediumWaitStone extends StoneExtension
	{
		public function MediumWaitStone()
		{
			super();
			name = '10s Cooldown';
			description = 'description';
			cooldown = 10 * 1000;
		}
	}
}