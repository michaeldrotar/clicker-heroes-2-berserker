package berserkerlib.stones.timing
{
	import berserkerlib.StoneExtension;

	public class ShorterWaitStone extends StoneExtension
	{
		public function ShorterWaitStone()
		{
			super();
			name = '3s Cooldown';
			description = 'description';
			cooldown = 3000;
		}
	}
}