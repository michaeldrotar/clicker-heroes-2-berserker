package berserkerlib
{
	import berserkerlib.Extension;
	import models.Character;
	import models.RubyPurchase;

	public class RubyPurchaseExtension extends Extension
	{
		public var name:String = '';
		public var description:String = '';
		public var iconId:Number = 1;
		public var price:Number = 0;
		public var priority:Number = 3;
		public var soldOutText:String = (new Character()).getDefaultSoldOutText();

		public var purchased:Boolean = false;

		public function RubyPurchaseExtension()
		{
			super();
		}

		public function get available():Boolean
		{
			return true;
		}

		public function purchase():void
		{

		}

		public function toRubyPurchase():RubyPurchase
		{
			var rubyPurchase:RubyPurchase = new RubyPurchase();
			rubyPurchase.name = name;
			rubyPurchase.getDescription = function():String
			{
				return description;
			};
			rubyPurchase.iconId = iconId;
			rubyPurchase.price = price;
			rubyPurchase.priority = priority;
			rubyPurchase.getSoldOutText = function():String
			{
				return soldOutText;
			};
			rubyPurchase.canAppear = function():Boolean
			{
				return available;
			};
			rubyPurchase.canPurchase = function():Boolean
			{
				return !purchased && available;
			};
			rubyPurchase.onPurchase = function():void
			{
				purchase();
				purchased = true;
			};
			return rubyPurchase;
		}

		public function onCharacterCreated():void
		{
			character.rubyPurchaseOptions.push(toRubyPurchase());
		}

		public function onRubyShopGenerating():void
		{
			purchased = false;
		}
	}
}