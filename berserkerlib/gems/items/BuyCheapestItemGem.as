package berserkerlib.gems.items
{
	import berserkerlib.ItemGemExtension;
	import models.Item;

	public class BuyCheapestItemGem extends ItemGemExtension
	{
		private var _cheapestItem:Item;

		public function BuyCheapestItemGem()
		{
			super();
			name = 'Buy Cheapest Item';
			description = 'Buys a random item as soon as its affordable.';
			assetNumber = 4;
		}

		public function get cheapestItem():Item
		{
			var item:Item;
			var cheapestItems:Array;
			if (!_cheapestItem || buyableItems.indexOf(_cheapestItem) === -1)
			{
				_cheapestItem = null;
				cheapestItems = [];
				for each (item in buyableItems)
				{
					if (cheapestItems.length === 0 || (cheapestItems[0] as Item).cost().eq(item.cost()))
					{
						cheapestItems.push(item);
					}
					else if (item.cost().lt((cheapestItems[0] as Item).cost()))
					{
						cheapestItems = [item];
					}
				}

				if (cheapestItems.length > 0)
				{
					_cheapestItem = cheapestItems[roller.integer(0, cheapestItems.length - 1)];
				}
			}
			return _cheapestItem;
		}

		override public function get ready():Boolean
		{
			return super.ready && cheapestItem && character.gold.gte(cheapestItem.cost());
		}

		override public function execute():void
		{
			if (cheapestItem)
			{
				buyItem(cheapestItem);
			}
		}
	}
}