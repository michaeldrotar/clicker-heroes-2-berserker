package berserkerlib.gems.skills
{
	import berserkerlib.SkillGemExtension;

	public class EviscerateSkillGem extends SkillGemExtension
	{
		public function onExtensionsCreated():void
		{
			skill = extensions.eviscerate.toSkill();
		}
	}
}