package berserkerlib
{
	import berserkerlib.Extension;
	import berserkerlib.Util;
	import models.Automator;

	public class Migrations extends Extension
	{
		public function onCharacterCreated():void
		{
			var versionKey:String = Util.toId([mod.modName, 'VERSION']);
			var characterVersion:Number = data.getNumber(versionKey) || mod.modVersion;

			if (characterVersion <= 0)
			{
				// Skill Tree used to wipe out all traits, but now using traits for things
				// other than talents so it only wipes talents.. wipe out all to start fresh
				character.traits = {};
			}
			if (characterVersion <= 1)
			{
				// Gem and Stone purchases used to cost 50 rubies each, now they cost 10
				character.rubies += (character.automator.stoneInventory.length + character.automator.gemInventory.length) * 40;
			}

			if (mod.modVersion > characterVersion || data.keys.indexOf(versionKey) === -1)
			{
				data.setNumber(versionKey, mod.modVersion);
			}

			cleanAutomator();
			cleanCatalog();
		}

		public function cleanAutomator():void
		{
			var id:String;
			var index:int;
			var automator:Automator = character.automator;

			var stoneInventory:Array = automator.stoneInventory;
			for (index = stoneInventory.length - 1; index >= 0; index--)
			{
				id = stoneInventory[index];
				if (!Automator.allStones[id])
				{
					character.rubies += 10 * character.automator.removeStoneFromSave(id);
				}
			}

			var gemInventory:Array = automator.gemInventory;
			for (index = gemInventory.length - 1; index >= 0; index--)
			{
				id = gemInventory[index];
				if (!Automator.allGems[id])
				{
					character.rubies += 10 * character.automator.removeGemFromSave(id);
				}
			}

			if (stoneInventory.length === 0 && gemInventory.length === 0)
			{
				character.hasUnlockedAutomator = false;
			}
		}

		public function cleanCatalog():void
		{
			var id:String;
			var index:int;

			var catalog:CatalogExtension = new CatalogExtension();
			var purchases:Array = catalog.purchases;
			for (index = purchases.length - 1; index >= 0; index--)
			{
				id = purchases[index];
				if (!Automator.allStones[id] && !Automator.allGems[id])
				{
					purchases.removeAt(index);
				}
			}
		}
	}
}