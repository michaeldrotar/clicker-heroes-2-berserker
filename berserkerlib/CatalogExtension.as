package berserkerlib
{

	public class CatalogExtension extends RubyPurchaseExtension
	{
		public var catalog:Object = {};

		public function CatalogExtension()
		{
			super();
		}

		public function get purchases():Array
		{
			var purchaseData:Array = data.getArray(Util.toId([mod.modName, 'CATALOG_PURCHASES']));
			if (!purchaseData)
			{
				purchaseData = [];
				data.setArray(Util.toId([mod.modName, 'CATALOG_PURCHASES']), purchaseData);
			}
			return purchaseData;
		}

		override public function get available():Boolean
		{
			var choice:String;
			for (choice in catalog)
			{
				if (purchases.indexOf(choice) === -1)
				{
					return true;
				}
			}
			return false;
		}

		override public function purchase():void
		{
			var choice:String;
			var choices:Object = {};
			for (choice in catalog)
			{
				if (purchases.indexOf(choice) === -1)
				{
					choices[choice] = catalog[choice];
				}
			}
			var selectedChoice:String = Util.weightedChoice(choices);
			purchaseChoice(selectedChoice);
			purchases.push(selectedChoice);
			data.setArray(id, purchases);
		}

		public function purchaseChoice(choice:String):void
		{

		}
	}
}