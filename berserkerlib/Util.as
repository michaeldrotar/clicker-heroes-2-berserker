package berserkerlib
{
	import heroclickerlib.CH2;
	import heroclickerlib.managers.Trace;
	import it.sephiroth.gettext._;
	import models.Character;
	import models.Monster;

	// Wish I had a better way to do this, import Trace doesn't work great.. might as well add other misc things for now.
	public class Util
	{
		public static const MELEE_RANGE:Number = 90;
		public static const MONSTER_POSITION_DISTANCE:Number = 176;
		public static const YARDS_DISTANCE:Number = 30;

		public static function monstersInRange(character:Character, minRange:Number, maxRange:Number):Array
		{
			var monster:Monster = null;
			var monsters:Array = [];
			for (var i:int = 0; i < CH2.world.monsters.monsters.length; i++)
			{
				monster = CH2.world.monsters.monsters[i];
				if (monster.isAlive && monster.y >= character.y + minRange && monster.y <= character.y + maxRange)
				{
					monsters.push(monster);
				}
				else if (monsters.length > 0)
				{
					break;
				}
			}
			return monsters;
		}

		public static function noop(... args):*
		{

		}

		public static function returnFirstArg(first:*, ... args):*
		{
			return first;
		}

		public static function toId(value:*):String
		{
			var text:String = '';
			if (value is Array)
			{
				text = (value as Array).join('_');
			}
			else
			{
				text = value.toString();
			}
			return underscore(text).toUpperCase();
		}

		public static function trace(... args):void
		{
			var arg:*;
			var key:String;
			var message:String;
			for (var i:int = 0; i < args.length; i++)
			{
				arg = args[i];
				if (arg is Object)
				{
					message = arg.toString();
					for (key in arg)
					{
						message += '\n    ' + key + '=' + arg[key];
					}
					args[i] = message;
				}
			}
			Trace.apply(null, args);
		}

		public static function underscore(value:String):String
		{
			return value.replace(/([a-z0-9])([A-Z])/g, '$1_$2').replace(/[^a-z0-9]+/gi, '_').replace(/(^_+|_+$)/g, '').toLowerCase();
		}

		public static function weightedChoice(choices:Object):*
		{
			var key:* = null;
			var sum:int = 0;
			var keys:Array = new Array();
			for (key in choices)
			{
				if (!(choices[key] is int))
				{
					throw new Error("Random.weightedChoice received bad values");
				}
				sum = sum + choices[key];
				keys.push(key);
			}
			keys.sort(Array.CASEINSENSITIVE);
			if (sum == 0)
			{
				throw new Error("Sum of weightedChoice options does not add up to anything meaningful");
			}
			var choice:Number = CH2.roller.modRoller.rand() % sum + 1;
			sum = 0;
			for each (key in keys)
			{
				sum = sum + choices[key];
				if (choice <= sum)
				{
					return key;
				}
			}
			throw new Error("Argument passed to Random.weightedChoice is invalid");
		}
	}
}