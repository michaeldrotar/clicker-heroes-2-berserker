package berserkerlib.skills
{
	import berserkerlib.SkillExtension;

	public class Eviscerate extends SkillExtension
	{
		public var damagePerEnergyMultiplier:Number;
		public var targetHealthThreshold:Number;

		public function Eviscerate()
		{
			name = 'Eviscerate';
			description = 'Consumes up to 100 energy, deals { damagePerEnergyMultiplier | percent | damage } damage per energy consumed.\n';
			description += 'Only usable on targets beneath { targetHealthThreshold | percent | value } health.';
			iconId = 163;
			cooldown = 10 * 1000;
			energyCost = 20;
			learnAtLevel = 20;

			damagePerEnergyMultiplier = 1;
			targetHealthThreshold = 0.2;
		}

		public function effectFunction():void
		{

		}
	}
}