package berserkerlib.rubyShop.bonuses
{
	import berserkerlib.RubyPurchaseExtension;
	import ui.IdleHeroUIManager;

	public class SkillPointBonus extends RubyPurchaseExtension
	{
		public function SkillPointBonus()
		{
			super();
			name = 'Skill Point Bonus';
			description = 'Awards a "free" skill point';
			iconId = 1;
			price = 40;
			priority = 3;
		}

		override public function purchase():void
		{
			character.totalStatPointsV2++;
			character.hasNewSkillTreePointsAvailable = true;

			// Copied from Character#addExperience
			if (IdleHeroUIManager.instance.mainUI)
			{
				IdleHeroUIManager.instance.mainUI.mainPanel.graphPanel.redrawGraph();
				if (IdleHeroUIManager.instance.mainUI.mainPanel.isOnGraphPanel)
				{
					IdleHeroUIManager.instance.mainUI.mainPanel.graphPanel.updateInteractiveLayer();
				}
			}
		}
	}
}