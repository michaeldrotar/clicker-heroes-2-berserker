package berserkerlib.rubyShop.catalogs
{
	import berserkerlib.StoneCatalogExtension;

	public class TimingStoneCatalog extends StoneCatalogExtension
	{
		public function TimingStoneCatalog()
		{
			super();
			name = 'Timing Stone';
			description = 'Purchase a random stone that activates based on timing.';
		}

		public function onExtensionsCreated():void
		{
			catalog[extensions.alwaysStone.id] = 10;
			catalog[extensions.shortestWaitStone.id] = 15;
			catalog[extensions.shorterWaitStone.id] = 20;
			catalog[extensions.shortWaitStone.id] = 25;
			catalog[extensions.mediumWaitStone.id] = 30;
			catalog[extensions.longWaitStone.id] = 25;
			catalog[extensions.longerWaitStone.id] = 20;
			catalog[extensions.longestWaitStone.id] = 15;
		}
	}
}
