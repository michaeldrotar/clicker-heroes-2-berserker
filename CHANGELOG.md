## 0.6.0 (TBD)

### General
- Items now only provide gold, treasure chest, and clickable benefits.

> **Comments:** Item stats aren't technically moddable yet, but I came up with a workaround for now that lets me remove choices that could be considered broken. Mana stats were removed because the Berserker doesn't use mana. Click damage didn't make sense since the Berserker doesn't click (though I was using it as a skill damage buff to avoid it being a waste). Crit had unintended scaling with Precision. Energy was always supposed to have a hard cap at 100 and made Berserk management trivial with high amounts. Haste was also removed because long-term I'd like haste to be a gild-related stat. After all that, the only things left were gold, treasure chests, and clickables. Once moddable, I'd probably remove the gold boosts as well and instead boost things like Power, Speed, Precision, overall skill damage, and possibly skill-related benefits like greater shove distance or more Bash energy.


## 0.5.0 (2018-10-14)
This release is targeting significant balance and gameplay improvements. The intention is to make skills more meaningful and fun to use and to bring the Berserker closer to its intended design.

### General
- The minimum GCD has been removed
- Leveling now grants 40% additional damage, up from 20%

### Skills

#### Bash
- Cooldown reduced from 7 seconds to 3 seconds
- Now dashes to the target if used outside of Cid's attack range

### Berserk
- Additional auto-attacks reduced from 2-3 to 1-2
- Additional auto-attacks now grant energy

> **Comments:** These changes are primarily targeted at helping Speed Berserkers find in a niche in a more active playstyle that relies on heavy skill usage.

#### Cleave
- Cooldown reduced from 10 seconds to 0 seconds (still affected by GCD)
- Cost changed from 5 mana to 25 energy
- Damage increased from 140% (+40% per additional monster) to 300% (+300% per additional monster)
- Now dashes to the target if used outside of Cid's attack range

> **Comments:** Mana was never intended to be part of the Berserker's toolkit. It was used because there's no way to remove it from the game currently, but items give access to lots of stats that I wish weren't modifiable (like max energy) so until items and resources become moddable there will have to be some less than desirable aspects to things.

#### Dash
- Removed

> **Comments:** Dash was an interesting way to generate energy, but it introduced two major problems: skills weren't usable if auto-attacks were one-shotting monsters due to their max range and the Automator was too slow to trigger Dash for starting characters in the very brief window that it was usable when running to the next monster.
To remedy this, the skill has been removed and all skills now dash (without the energy generation) when the monster is out of range. This also opens the possibility for fun interactions like Shove could be extra powerful when Cid has a running start from dashing.
Players that had purchased the Dash skill gem should receive a 50 ruby refund (40 for the price reduction + 10 for the gem itself).

#### Shove
- Cooldown reduced from 8 seconds to 0 seconds (still affected by GCD)
- Cost reduced from 50 energy to 20 energy
- Damage increased from 100% to 300%
- Shove distance no longer reduced by number of monsters shoved
- Now dashes to the target if used outside of Cid's attack range

> **Comments:** Shove no longer scales with Power, but will consistently push monsters 3 spaces, making it more predictable so players can more easily plan a Shove/Cleave rotation.

### Stats

#### Crit
- Damage is now multiplied by crit damage for every 100% crit chance

> **Comments:** It was difficult to balance crit chance vs crit damage because crit chance had an effective hardcap at 100% where anything additional was wasted. This change won't be reflected on the stats panel but occurs during damage calculation so a player with 100 base damage, 230% crit chance, and 200% crit damage will now do 400 base damage (100 * 2 * 2) and have a 30% chance to crit for 800 damage.

#### Power
- No longer affects shove distance

> **Comments:** See Shove for explanation.

#### Precision
- Crit chance increased from 0.5% to 1%
- Crit damage multiplier increased from 120% to 125%

> **Comments:** These changes should make Precision a little more predictable but still spiky and thus potentially stronger than a pure Power Berserker. Combined with the Crit changes, pure Precision Berserkers should be able to exceed 100% crit chance.

#### Speed
- Now also reduces gcd by 0.01 seconds
- Adds a linear 0.3 additional auto-attacks to Berserk instead of multiplying by 1%
- Berserk movement speed bonus increased from 2.5% to 3%

> **Comments:** Seeing lots of auto-attacks was fun to watch but significantly underperformed compared to Power or Precision damage. These changes as well as changes to Shove and Berserk give Speed Berserkers a more active playstyle that should be able to outperform more passive builds.

### Ruby Shop

#### General
- Reduced price of gem and stone catalogs from 50 rubies to 10 rubies

> **Comments:** Starting a new character felt like too long of a wait before the automator became available, and the inherent randomness meant it could be even longer before it was really useful.
Each golden fish providing 1 purchase feels about right and increasing the odds on earlier skills lessens the chance of unlocking skill gems before you've even unlocked their skill.
Players should receive a 40 ruby refund for each gem and stone they had previously bought.

### Bonuses
- Reduced cost of Level Up Bonus from 150 rubies to 70 rubies
- Reduced cost of Skill Point Bonus from 100 rubies to 40 rubies
- Added Skill Tree Reset for 100 rubies to allow re-picking all stats and talents from the skill tree

#### Item Gem Catalog
- Added gem to buy cheapest item (if multiple items are tied for cheapest, buys one at random)
- Added gem to upgrade cheapest item up to level 50
- Added gem to upgrade cheapest item up to level 100

#### Skill Gem Catalog
- Re-weighted skill gem catalog to highly favor giving earlier skills before later ones

### Bug Fixes
- Ruby shop should now only appear if you have at least 10 rubies
- Cleave's additional damage should now work properly


## 0.4.1 (2018-10-12)

### Bug Fixes
- Fix power and precision stats to properly adjust stat levels immediately on purchase


## 0.4.0 (2018-10-10)

### Major Changes
- Ruby Shop will now appear every minute, down from 5 minutes
- Precision now also grants 0.5% crit chance
- Power now grants an additional 5% damage
- Leveling now grants an additional 5% damage
- Berserk now has a baseline 2-3 additional auto-attacks (which will improve Speed scaling)

### New Additions
- Level Ups can be purchased from the Ruby Shop
- Skill Points can be purchased from the Ruby Shop

### Bug Fixes
- Game should no longer crash when creating a new Berserker


## 0.3.0 (2018-10-09)
This release introduces the start of the Ruby Shop and Automator changes.
Automator stones and gems will be purchased via catalogs in the Ruby Shop.
For instance, the Timing Stone Catalog will grant a stone that activates
based on timing.
Catalogs are weighted such that the stronger options will generally require
more purchases in the catalog in order to reach them but lucky players may
reach them sooner.
Each stone or gem can only be purchased once per gild so it may be
beneficial to delay gilding until you get that stone or gem that you've
been trying for.

### Major Changes
- Removed all default items from the Ruby Shop

### New Additions
- Added Timing Stone Catalog to the Ruby Shop
- Added Skill Gem Catalog to the Ruby Shop

### Bug Fixes
- The game should no longer lock up when approaching the final boss


## 0.2.0 (2018-09-29)
The Berserk mechanic was meant to add depth to the gameplay by balancing raw strength vs speed and making the "always upgrade items" strategy have an alternative,
but it ended up feeling punishing when a new item meant you were killing too fast to generate energy.
This release focuses on making the gameplay feel better and making it exciting to max energy without punishing you for spending energy or upgrading items.

### Major Changes
- Berserk no longer drains energy and now provides a consistent chance for additional auto-attacks and a consistent movement speed bonus, effects are improved if you maintain a certain energy threshold
- Bash now deals additional damage when energy is full
- Shove has been simplified to push all monsters up to a max distance, instead of pushing max monsters up to a max distance
- Replaced Endurance with Speed, which increases number of auto-attacks and move speed from Berserk
- Replaced Mastery with Precision, which increases crit damage
- Power now increases all damage and Shove distance

### New Additions
- The Rage talent generates additional energy over time each time Bash is used
- The Fury talent causes Berserk to generate energy while in combat

### Bug Fixes
- Berserk's auto-attacks no longer slow down the normal pacing of auto-attacks
- Shove's distance now scales down dynamically as new monsters are added to the stack


## 0.1.1 (2018-09-17)
- Support v0.08e

## 0.1.0 (2018-09-16)
- Add Berserk
- Add Bash
- Add Shove
- Add Cleave
- Add Dash
